from telegram import Update, Bot, Message, CallbackQuery, User
from enum import Enum
from bot.models import State, Cache, Member, Cost, Share, Market
from bot.bot_messages import *
from bot.bot_functions import *


def addmarket_handle_command(bot: Bot, message, pvcommand):
    state = State(group_id=message.chat.id, user_id=message.from_user.id,
                  last_command=pvcommand.value, command_state=0)
    state.save()
    send_message_select_type(bot, message.chat.id)


def addmarket_handle_message(bot: Bot, data, state):
    if state.command_state == 0:
        addmarket_state_0(bot, data, state)
    elif state.command_state == 1:
        addmarket_state_1(bot, data, state)
    elif state.command_state == 2:
        addmarket_state_2(bot, data, state)
    elif state.command_state == 3:
        addmarket_state_3(bot, data, state)
    elif state.command_state == 4:
        addmarket_state_4(bot, data, state)
    elif state.command_state == 5:
        addmarket_state_5(bot, data, state)


def addmarket_state_0(bot: Bot, data, state):
    if not isinstance(data, CallbackQuery):
        send_message_not_a_callback(bot, data.chat.id)
        return
    chat_id = data.message.chat.id
    cache = Cache(group_id=chat_id, user_id=data.from_user.id,
                  var_name="market_type", string_value=data.data)
    cache.save()
    state.command_state = 1
    state.save()
    send_message_send_name(bot, chat_id)


def addmarket_state_1(bot: Bot, data, state):
    if not isinstance(data, Message):
        send_message_not_a_message(bot, data.message.chat.id)
        return
    message = data
    cache = Cache(group_id=message.chat.id, user_id=message.from_user.id,
                  var_name="market_name", string_value=message.text)
    cache.save()
    state.command_state = 2
    state.save()
    send_message_send_desc(bot, message.chat.id)


def addmarket_state_2(bot: Bot, data, state):
    if not isinstance(data, Message):
        send_message_not_a_message(bot, data.message.chat.id)
        return
    message = data
    cache = Cache(group_id=message.chat.id, user_id=message.from_user.id,
                  var_name="market_desc", string_value=message.text)
    cache.save()
    state.command_state = 3
    state.save()
    send_message_send_location(bot, message.chat.id)


def addmarket_state_3(bot: Bot, data, state):
    if not isinstance(data, Message):
        send_message_not_a_message(bot, data.message.chat.id)
        return
    message = data
    if message.location is None:
        send_message_not_a_location(bot, message.chat.id)
        return
    cache = Cache(group_id=message.chat.id, user_id=message.from_user.id,
                  var_name="market_latitude", value=message.location[0])
    cache.save()
    cache = Cache(group_id=message.chat.id, user_id=message.from_user.id,
                  var_name="market_longitude", value=message.location[1])
    cache.save()
    state.command_state = 4
    state.save()
    send_message_send_photo(bot, message.chat.id)


def addmarket_state_4(bot: Bot, data, state):
    if not isinstance(data, Message):
        send_message_not_a_message(bot, data.message.chat.id)
        return
    message = data
    if message.photo is None or len(message.photo) == 0:
        send_message_not_a_photo(bot, message.chat.id)
        return
    cache = Cache(group_id=message.chat.id, user_id=message.from_user.id,
                  var_name="market_photo", string_value=message.photo)
    cache.save()
    state.command_state = 5
    state.save()
    send_message_select_purchase_type(bot, message.chat.id)


def addmarket_state_5(bot: Bot, data, state):
    if not isinstance(data, CallbackQuery):
        send_message_not_a_callback(bot, data.chat.id)
        return
    chat_id = data.message.chat.id
    name_cache = None
    desc_cache = None
    type_cache = None
    latitude_cache = None
    longitude_cache = None
    photo_cache = None
    try:
        name_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                       var_name="market_name")
        type_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                       var_name="market_type")
        desc_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                       var_name="market_desc")
        latitude_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                           var_name="market_latitude")
        longitude_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                            var_name="market_longitude")
        photo_cache = Cache.objects.get(group_id=chat_id, user_id=data.from_user.id,
                                        var_name="market_photo")
    except Cache.DoesNotExist:
        send_message_cache_not_found(bot, chat_id)
        return
    if name_cache is None or type_cache is None or desc_cache is None \
            or latitude_cache is None or longitude_cache is None or photo_cache is None:
        send_message_cache_not_found(bot, chat_id)
        return
    market = Market(type=int(type_cache.string_value), name=name_cache.string_value,
                    desc=desc_cache.string_value, latitude=latitude_cache.value,
                    longitude=longitude_cache.value, purchase_type=int(data.data),
                    photo=photo_cache.string_value)
    market.save()
    send_message_market_saved(bot, chat_id, data.data)
    delete_states_and_caches(chat_id, data.from_user.id)
