from math import sqrt, log

from telegram import Update, Bot, Message, CallbackQuery, User
from enum import Enum
from bot.models import State, Cache, Member, Cost, Share, Market
from bot.bot_messages import *
from bot.bot_functions import *


class MarketScore:
    def __init__(self, market, location_score=0, sort_score=0):
        self.market = market
        self.location_score = location_score
        self.sort_score = sort_score


def showmarkets_handle_command(bot: Bot, message, command):
    state = State(group_id=message.chat.id, user_id=message.from_user.id,
                  last_command=command.value[0], command_state=0)
    state.save()
    send_message_select_type(bot, message.chat.id, is_for_customers=True)


def handle_showmarket_reply(bot: Bot, data, state):
    if state.command_state == 0:
        showmarket_state_0(bot, data, state)
    elif state.command_state == 1:
        showmarket_state_1(bot, data, state)


def showmarket_state_0(bot: Bot, data, state):
    if not isinstance(data, CallbackQuery):
        send_message_not_a_callback(bot, data.chat.id)
        return
    chat_id = data.message.chat.id
    cache = Cache(group_id=chat_id, user_id=data.from_user.id,
                  var_name="showmarket_type", string_value=data.data)
    cache.save()
    state.command_state = 1
    state.save()
    send_message_send_location_showmarket(bot, chat_id, data.data)


def showmarket_state_1(bot: Bot, data, state):
    if not isinstance(data, Message):
        send_message_not_a_message(bot, data.chat.id)
        return
    message = data
    if message.location is None:
        send_message_not_a_location(bot, message.chat.id)
        return
    type_cache = None
    try:
        type_cache = Cache.objects.get(group_id=message.chat.id, user_id=message.from_user.id,
                                       var_name="showmarket_type")
    except Cache.DoesNotExist:
        send_message_cache_not_found(bot, message.chat.id)
        return
    if type_cache is None:
        send_message_cache_not_found(bot, message.chat.id)
        return
    type = type_cache.string_value
    markets = Market.objects.filter(type=int(type))
    sorted_markets = sort_markets(markets, message.location[0], message.location[1])
    # bot.sendMessage(message.chat.id, str(message.location[0]) + " " + str(message.location[1]))
    send_message_markets(bot, message.chat.id, sorted_markets)
    delete_states_and_caches(message.chat.id, message.from_user.id)


def sort_markets(markets, latitude, longitude):
    new_markets = []
    for market in markets:
        new_markets.append(MarketScore(market))
    for market in new_markets:
        market.location_score = sqrt(
            (market.market.latitude - latitude) ** 2 + (market.market.longitude - longitude) ** 2)
        market.sort_score = market.market.purchase_type / log(market.location_score)
    sorted_markets = sorted(new_markets, key=lambda x: x.sort_score, reverse=True)
    result = []
    for market in sorted_markets:
        result.append(market.market)
    return result