import telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot, Message
from emoji import emojize
from bot.bot_functions import MarketType, market_type_names, \
    MarketPurchaseTypes, market_purchase_type_name, market_purchase_type_prices


def send_message_send_cost(bot, id):
    try:
        bot.sendMessage(id, "لطفا مقدار هزینه را وارد کنید")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_command_not_found(bot, id):
    bot.sendMessage(id, "هنو این کامندو اضافه نکردیم!")


def send_message_already_registered(bot, id):
    try:
        bot.sendMessage(id, emojize("you have already registered sir! :man_in_tuxedo: :kiss:", use_aliases=True))
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_private_chat(bot, id):
    try:
        bot.sendMessage(id, "من با تو صحبتی ندارم.")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_have_not_selected_users(bot, id):
    try:
        bot.sendMessage(id, "دِ خب یکیو انتخاب کن!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_canceled(bot, id):
    try:
        bot.sendMessage(id, "کنسله!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_remaining(bot, id, value):
    try:
        bot.sendMessage(id, "مقدار " + str(value) + " باقیمانده برای خود شما ثبت شد.")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_send_every_user_share(bot, id):
    try:
        bot.sendMessage(id, "هر کدوم چقد پول دادن؟")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_get_name(bot, id):
    try:
        bot.sendMessage(id, "چیکار کردی با این پول؟")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_mydebt(bot, id, result):
    try:
        if result > 0:
            bot.sendMessage(id, emojize("شما %d دلار بدهکار هستید :sunglasses:" % result, use_aliases=True))
        else:
            bot.sendMessage(id, emojize(" شما %d دلار طلبکار هستید :sunglasses: " % (-1 * result), use_aliases=True))
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_username(bot, id, username):
    try:
        bot.sendMessage(id, "" + username + " :")  # can add @ at first
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_user_registered(bot, id):
    try:
        bot.sendMessage(id, emojize("ای بابا تو رو کی راه داده؟! :man_facepalming:", use_aliases=True))
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_share_exceeded(bot, id):
    try:
        bot.sendMessage(id, "اینکه بیشتر از اون پولی که گفتی شد!\n برو خودتو اسکل کن")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_reply_without_state_error(bot, id):
    try:
        bot.sendMessage(id, "مگه من مسخره توام!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_not_a_number_error(bot, id):
    try:
        bot.sendMessage(id, "ببین چگونه جان مشوش است عدد بده!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_send_location(bot, id):
    try:
        bot.sendMessage(id, "لوکیشن محل خود را بفرستید")
    except telegram.error.BadRequest as e:
        print("error" + str(e))


def send_message_send_location_showmarket(bot, id, market_type):
    result = "برای پیدا کردن "
    result += market_type_names[int(market_type)]
    result += "‌های نزدیک خود لوکیشن محل خود را بفرستید"
    try:
        bot.sendMessage(id, result)
    except telegram.error.BadRequest as e:
        print("error" + str(e))


def send_message_not_a_location(bot, id):
    try:
        bot.sendMessage(id, "لطفا لوکیشن بفرستید!")
    except telegram.error.BadRequest as e:
        print("error" + str(e))


def send_message_not_a_message(bot, id):
    try:
        bot.sendMessage(id, "دکمه نزن بچه!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_not_a_callback(bot, id):
    try:
        bot.sendMessage(id, "دکمه بزن!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_cache_not_found(bot, id):
    try:
        bot.sendMessage(id, "برو بگو خودش بیاد!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_have_not_members(bot, id):
    try:
        bot.sendMessage(id, "هیچ کدام از شما لاشیا ثبت نام نکردید!")
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_added_cost(bot, id):
    try:
        bot.sendMessage(id, emojize("باتشکر :rose: :rose:", use_aliases=True))
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_select_cost_kind(bot, id):
    try:
        button_list = [
            InlineKeyboardButton("تقسیم مساوی بین همه", callback_data="0"),
            InlineKeyboardButton("تقسیم مساوی بین بعضی از افراد", callback_data="1"),
            InlineKeyboardButton("تقسیم دلخواه", callback_data="2")
        ]
        n_cols = 1
        menu = [button_list[i:i + n_cols] for i in range(0, len(button_list), n_cols)]
        reply_markup = InlineKeyboardMarkup(menu)
        bot.sendMessage(id, "نوع پرداخت هزینه را انتخاب کنید", reply_markup=reply_markup)
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_select_users(bot, id, users, selecteds, message_id=None):
    try:
        button_list = []
        for user in users:
            button_text = user.username
            if str(user.id) in selecteds:
                button_text += " - :white_check_mark: "
            button_list.append(InlineKeyboardButton(emojize(button_text, use_aliases=True), callback_data=user.id))
        button_list.append(InlineKeyboardButton("همینا بودن", callback_data="ended"))
        n_cols = 2
        menu = [button_list[i:i + n_cols] for i in range(0, len(button_list), n_cols)]
        reply_markup = InlineKeyboardMarkup(menu)
        if message_id is None:
            message = bot.sendMessage(id, "کدومشون بود؟", reply_markup=reply_markup)
            return message.message_id
        else:
            bot.edit_message_reply_markup(id, message_id, reply_markup=reply_markup)
    except telegram.error.BadRequest:
        print("error: telegram bad request")


def send_message_getalldebts(bot, id, payement):
    res = ''
    for pay in payement:
        res += str(pay[2]) + " :  " + pay[0] + ' --> ' + pay[1] + '\n' + \
               "شماره کارت: " + pay[3] + '\n'

    bot.sendMessage(id, res)


def send_message_select_type(bot, id, is_for_customers=False):
    button_list = []
    for type in MarketType:
        try:
            button_text = market_type_names[type.value[0]]
            button_list.append(
                InlineKeyboardButton(emojize(button_text, use_aliases=True), callback_data=str(type.value[0])))
        except Exception as e:
            print(e)
    n_cols = 1
    menu = [button_list[i:i + n_cols] for i in range(0, len(button_list), n_cols)]
    reply_markup = InlineKeyboardMarkup(menu)
    message = "نوع محل تفریحیِ خود را انتخاب کنید"
    if is_for_customers:
        message = "دنبال کدام نوع محل میگردید؟"
    bot.sendMessage(id, message, reply_markup=reply_markup)


def send_message_select_purchase_type(bot, id):
    button_list = []
    for type in MarketPurchaseTypes:
        try:
            button_text = market_purchase_type_name[type.value[0]]
            button_list.append(
                InlineKeyboardButton(emojize(button_text, use_aliases=True), callback_data=str(type.value[0])))
        except Exception as e:
            print(e)
    n_cols = 1
    menu = [button_list[i:i + n_cols] for i in range(0, len(button_list), n_cols)]
    reply_markup = InlineKeyboardMarkup(menu)
    message = "نوع طرح مورد نظر خود را انتخاب کنید"
    bot.sendMessage(id, message, reply_markup=reply_markup)


def send_message_send_name(bot, id):
    bot.sendMessage(id, "اسمت چیه عمو؟")


def send_message_send_desc(bot, id):
    bot.sendMessage(id, "توضیح بده!")


def send_message_market_saved(bot, id, purchase_type):
    result = "برای استفاده از طرح "
    result += market_purchase_type_name[int(purchase_type)]
    result += " مبلغ " + str(market_purchase_type_prices[int(purchase_type)])
    result += "تومان تا ۲۴ ساعت آینده ارسال کنید تا تبلیغ شما در لیست تبلیغ‌های بات قرار بگیرد."
    bot.sendMessage(id, result)
    bot.sendMessage(id, "مبلغ واریزیِ شما دریافت شد و محل به بات افزوده شد!")


def send_message_market(bot: Bot, id, market):
    result = market.name
    result += "\n\n"
    result += market.desc
    bot.send_photo(id, photo=market.photo, caption=result)


def send_message_market_location(bot: Bot, id, market):
    bot.send_location(id, latitude=market.latitude, longitude=market.longitude)


def send_message_markets(bot, id, markets):
    for i in range(len(markets)):
        market = markets[i]
        bot.sendMessage(id, "محل شماره %d" % (i + 1))
        send_message_market(bot, id, market)
        send_message_market_location(bot, id, market)
    bot.sendMessage(id, emojize(":heart: :heart:", use_aliases=True))


def send_message_not_a_photo(bot, id):
    bot.sendMessage(id, "لطفا عکس بفرستید!")


def send_message_send_photo(bot, id):
    bot.sendMessage(id, "برای محل خود عکس بفرستید")


def send_message_send_card_number(bot, id):
    bot.sendMessage(id, "شماره کارت خود را وارد کنید.\nشماره کارت باید ۱۶ رقمی باشد و بدون فاصله وارد شود")


def send_message_card_number_not_digit(bot, id):
    bot.sendMessage(id, "شماره کارت فقط باید از ارقام تشکیل شده باشد")


def send_message_card_number_low(bot, id):
    bot.sendMessage(id, "طول شماره کارت کمتر از ۱۶ رقم وارد شده")


def send_message_card_number_high(bot, id):
    bot.sendMessage(id, "طول شماره کارت بیشتر از ۱۶ رقم وارد شده")
